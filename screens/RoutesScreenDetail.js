import React from 'react';
import { View, Text, SafeAreaView, StyleSheet } from 'react-native';

import CustomHeader from '../components/CustomHeader';
import { TouchableOpacity } from 'react-native-gesture-handler';


export default function RoutesScreenDetail ({route, navigation}) {
    const { name } = route.params;
    const { email } = route.params;
    const { cell } = route.params;
    const { location } = route.params;
    console.log('receiving props =========> ', email);

    const handlePressDetail = () => {
        navigation.navigate('HomeDetail', {
            name,
            email,
            cell,
        })
    }
    return(
        <SafeAreaView style={styles.container}>
           <CustomHeader title= 'DETALLES DE VENTA' navigation={navigation}/>
           <View style={{flexDirection: 'row'}}>
            <View style={{backgroundColor: '#fff', height: 50, width:100, borderRadius: 5 }}>
                <Text style={{textAlign:'center', color:'gray'}}> fondo banca</Text>
                <Text style={{textAlign:'center',padding: 5, fontWeight: 'bold'}}> 0000 </Text>
            </View>
            <View style={{justifyContent: 'center', width: 150}}>
                <Text style={{fontSize: 16, color:'#ffff', textAlign: 'center'}}>BANCA:{name}</Text>
            </View>
            <TouchableOpacity style={{
                                    paddingTop: 14,
                                    }}
                              onPress={handlePressDetail}      
            >
            <View style={{justifyContent: 'center', width: 100}}>
                <Text style={{fontSize: 16, color:'#ffff', textAlign: 'center'}}>Ir a la info</Text>
            </View>
            </TouchableOpacity>
           </View>
            <View style={styles.list}>
              <Text style={{fontSize: 16, marginTop: 10, color:'#ffff'}}>VENTAS</Text>
                <View style={styles.sale}>
                    <Text style={{textAlign: 'center', fontSize: 32, color: 'blue'}}>{location}</Text>
                </View>

                    <Text style={styles.text1}>BALANCE EN CAJA</Text>
                <View style={styles.balance} >
                    <Text style={styles.text2}>0.000.000</Text>
                </View>

                    <Text style={styles.text1}>MONTO A RETIRAR</Text>
                <View style={styles.balance}>
                    <Text style={styles.text2}>0.000.000</Text>
                </View>
 
                    <Text style={styles.text1}>MONTO A DEJAR</Text>
                <View style={styles.balance}>
                    <Text style={styles.text2}>0.000.000</Text>
                </View>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#393939'
    },
    list: {
        flex: 1, 
        alignItems: 'center', 
        paddingTop: 10
    },
    sale: {
        backgroundColor: '#E5E7E9', 
        justifyContent: 'center', 
        width: 250, 
        height: 80, 
        marginTop: 10, 
        borderRadius: 5
    },
    balance: {
        width: 250, 
        height: 50, 
        backgroundColor: '#E5E7E9', 
        marginTop: 10, 
        justifyContent: 'center', 
        borderRadius: 5
    },
    text1: {
        textAlign: 'center', 
        fontSize: 16, 
        marginTop: 15,
        color:'#ffff'
    },
     text2: {
        textAlign: 'center', 
        fontSize: 16,
        color: 'blue',
     }
})