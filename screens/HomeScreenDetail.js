import React from 'react';
import { View, Text, Button, SafeAreaView, StyleSheet, } from 'react-native';
import CustomHeader from '../components/CustomHeader';
//import MapContainer from '../containers/MapContainer';
import MapView, { PROVIDER_GOOGLE} from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
//import Geocoder from 'react-native-geocoding';
//import { AnimatedRegion } from 'react-native-maps';
//import { SafeAreaView } from 'react-native-safe-area-context';



export default function HomeScreenDetail ({route, navigation}) { 
    const { name } = route.params;
    const { email } = route.params;
    const { cell } = route.params;
    const origin = {latitude: 18.518679, longitude: -69.996774, latitudeDelta: 0.1000,
        longitudeDelta: 0.1000};
const destination = {latitude: 18.477904,
    longitude: -69.957315, latitudeDelta: 0.1000,
    longitudeDelta: 0.1000};
    // const { params } = navigation.props;
    // const { name } = route.params;
    // console.log('receiving props ==>', {route});
  
    return(
        <SafeAreaView style={{ flex: 1 }}>
           <CustomHeader title= 'Home Detail' navigation={navigation}/>
            <View style={{ alignItems: 'center', backgroundColor:'#FFF' }}>
              
                <View style={{justifyContent: 'center', paddingHorizontal: 0, backgroundColor:'#D6DBDF', width: 375, height: 550 }}>
                    <View style={{height: 100, justifyContent: 'center', paddingHorizontal: 25, backgroundColor: '#393939'}}>
                        <Text style={styles.textInf}>BANCA</Text>
                        <Text style={styles.textInf}>NOMBRE: {name}</Text>
                        <Text style={styles.textInf}>DIRECCION: {email}</Text>
                        <Text style={styles.textInf}>CELL: {cell}</Text>
                    </View>
                    <MapView
                        style={{ flex: 1 }}
                        provider={PROVIDER_GOOGLE}
                        showsUserLocation={true}
                        showsMyLocationButton={true}
                        followsUserLocation={true}
                        AnimatedRegion={true}
                        initialRegion={{
                        latitude: 18.518679,
                        longitude: -69.996774,
                        latitudeDelta: 0.1000,
                        longitudeDelta: 0.1000}} 
                        
                        >

  < MapViewDirections 
    origin = { origin } 
    destination = { destination } 
    apikey = { 'AIzaSyBLvqS5-5SDLFBJ5x-OV069ed6bh-UB4gk' } 
    strokeWidth = { 4 } 
    strokeColor = "red" 
    mode = {"DRIVING"}
    
  /> 

                    <MapView.Marker
                        coordinate={{latitude: 37.78825,
                        longitude: -122.4324}}
                        title={name}
                        description={"VENTA: RD$10,000"}
                        image={require('../images/sb.png')} />
                      </MapView>
                </View>
                </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create ({
    textInf: {
        fontSize: 16,
        color: '#ffff'
    }
})