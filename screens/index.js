import React from 'react-native';
import Screen from './AccountScreen';

export const ProfileScreen = ({navigation}) => <Screen navigation={navigation} name= 'Profile'/>
export const StatusScreen = ({navigation}) => <Screen navigation={navigation} name= 'Status'/>
export const MessageScreen = ({navigation}) => <Screen navigation={navigation} name= 'Message'/>
export const StatisticScreen = ({navigation}) => <Screen navigation={navigation} name= 'Statistics'/>
export const TransactionsScreen = ({navigation}) => <Screen navigation={navigation} name= 'Transanctions'/>
export const ReportScreen = ({navigation}) => <Screen navigation={navigation} name= 'Reports'/>
export const AccountScreen = ({navigation}) => <Screen navigation={navigation} name= 'Account'/>