import React, { useEffect, useState } from 'react';
import { ActivityIndicator, ScrollView, View } from 'react-native';
import { Body, 
         Radio, 
         Button, 
         Text, 
         List, 
         ListItem, 
         Left,
         Right, 
         } from 'native-base';
import CustomHeader from '../components/CustomHeader';
//import Search from '../components/Search';
import { FlatList  } from 'react-native-gesture-handler';
import { SearchBar } from 'react-native-elements';




function Item({ name, email, location, handlePress, Pressed, selected }) {
    return (
        <ScrollView>
            <ListItem avatar>
            <Left>
                <Radio selected={selected} onPress={Pressed} style={{alignItems:'center'}}/>
            </Left>
            <Body>
                <Text>{name}</Text>
                <Text note>{email}</Text>
                <Text note>{location}</Text>
            </Body>
            <Right>
                <Button rounded danger onPress={handlePress}>
                    <Text>Info</Text>
                </Button>
            </Right>
            </ListItem>
        </ScrollView>
    );
  }
  


export default function HomeScreen ({navigation}) {
    const [pressedOn, setPressedOn] = useState(false);
    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    const [selected, setSelected] = useState(false);
    const [fullData, setFullData] = useState([]);
    const [search, setSearch] = useState('');
    
    console.log('recibiendo todos los datos', fullData);
  
    useEffect(() => {
        const abortCotroller = new AbortController()
        const signal = abortCotroller.signal
      fetch('https://randomuser.me/api?results=20',{signal: signal})  //https://jsonplaceholder.typicode.com/photos
        .then((response) => response.json())
        .then((json) => {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                
            setData(json.results)
            setFullData(json.results)
        })
        .catch((error) => console.error(error))
        .finally(() => setLoading(false));

        return function cleanup() {
            abortCotroller.abort()
        }
    }, []);

   const handlePress = ({item}) => {
       if(!pressedOn) {
           setPressedOn(true);
        navigation.navigate('HomeDetail', {
            name:item.name.first, 
            email:item.email, 
            cell:item.cell,
        })
    }
   }
    const Pressed = () => {
         setSelected(true);
    };


    function SearchFilterFunction(text) {
        console.log(text);
        //passing the inserted text in textinput
        const newData = fullData.filter(function(item) {
          //applying filter for the inserted text in search bar
          const itemData = item.name.first ? item.name.first.toUpperCase() : ''.toUpperCase();
          const textData = text.toUpperCase();
          return itemData.indexOf(textData) > -1;
        });

        setData(newData);
        setSearch(text);
        

    }

    return (
        <View style={{ flex: 1, backgroundColor:'#fff'}}>
            <CustomHeader title= 'RUTAS' isHome={true} navigation={navigation}/>
                <SearchBar
                    round
                    searchIcon={{ size: 24 }}
                    onChangeText={text => SearchFilterFunction(text)}
                    onClear={text => SearchFilterFunction ('')}
                    placeholder="Type Here..."
                    value={search}
                />
                <View style={{marginBottom: 60, marginTop: 15}}>
                <List>
                {isLoading ? <ActivityIndicator animating size= 'large'/> 
                : (
                    <FlatList
                        data={data}
                        renderItem={({ item }) => (
                            <Item
                                name={item.name.first}
                                email={item.email}
                                cell={item.cell}
                                handlePress={() => handlePress({item})}  
                                // Pressed={Pressed}
                                // selected={selected}    
                            />                  
                        ) 
                        }
                        keyExtractor={(item, index) => index.toString()}                                     
                    />
                )}
                </List>
                </View>
        </View>
    );
}