import React from 'react';
import { View, Text, SafeAreaView, StyleSheet} from 'react-native';
import CustomHeader from '../components/CustomHeader';
import { FlatList } from 'react-native-gesture-handler';
//import Date from '../components/Date';
import moment from 'moment'; 



export default function AccountScreenDetail ({navigation}) {
    var currentDate = moment().format("DD/MM/YYYY");
    console.log(currentDate);
    return(
        <SafeAreaView style={{ flex: 1 }}>
           <CustomHeader title= 'Account Detail' navigation={navigation}/>
           <View style={styles.container}>
               <View style={{flexDirection:'row', height: 30}}>
                    <View style={{width: 100, backgroundColor: 'red'}}>
                        <Text>Fecha</Text>
                    </View>
                    <View style={{width: 150, backgroundColor: 'blue'}}>
                        <Text>name</Text>
                    </View>
                    <View style={{width: 55, backgroundColor: 'red'}}>
                        <Text>entrada</Text>
                    </View>
                    <View style={{width: 55, backgroundColor: 'blue'}}>
                        <Text>salida</Text>
                    </View>
               </View>
                <FlatList
                data={[
                    {key: '1', name:'Devin' },
                    {key: '2', name:'Juan'},
                    {key: '3', name:'David'},
                    {key: '4', name:'Pedro'},
                    {key: '5', name:'Ana'},
                    {key: '6', name:'Jose'},
                    {key: '7', name:'Luis'},
                    {key: '8', name:'Chris'},
                    {key: '9', name:'Jhonson'},
                    {key: '10', name:'Peter'},
                    {key: '11', name:'Carlos'},
                ]}
                // numColumns={1}
                renderItem={({item}) =>
                    <View style={{flexDirection: 'row'}}> 
                        <View style={{width: 100}}>
                            <Text style={{backgroundColor: 'gray'}}>{currentDate}</Text>
                        </View> 
                        <View style={{width: 150}}>
                            <Text style={{backgroundColor: 'yellow'}}>{item.name}</Text>
                        </View>
                        <View style={{backgroundColor: 'orange', width: 55, alignContent:'center', alignItems: 'center'}}>
                            <Text>{item.key}</Text>
                        </View>   
                        <View style={{backgroundColor: 'green', width: 55, alignContent:'center', alignItems: 'center'}}>
                            <Text>{item.key}</Text>
                        </View>
                    </View> 
                }
                />
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    container: {
     flex: 1,
    },
    item: {
      padding: 10,
      fontSize: 18,
      height: 44,
      width: 150
    },
  })
  