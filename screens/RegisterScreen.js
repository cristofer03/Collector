import React from 'react';
import { View, Text, SafeAreaView, TouchableOpacity } from 'react-native';
import CustomHeader from '../components/CustomHeader';


export default function RegisterScreen ({navigation}) {
    return(
        <SafeAreaView style={{ flex: 1 }}>
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text>Register Screen</Text>
                <TouchableOpacity
                style={{marginTop: 20}}
                onPress={() => navigation.navigate('GroupDetail') }
                >
                   <Text>Registro</Text>
                </TouchableOpacity>

                <TouchableOpacity
                style={{marginTop: 20}}
                onPress={() => navigation.navigate('Login') }
                >
                   <Text>Ir al Login</Text>
                </TouchableOpacity>
            </View>
        </SafeAreaView>
    );
}