import React from 'react';
import { 
    View, 
    Text, 
    SafeAreaView, 
    TouchableOpacity, 
    Image, 
    ImageBackground, 
    TextInput,
    Dimensions,
} from 'react-native';
import { Input } from 'native-base';
import Icon from 'react-native-vector-icons/Ionicons'



export default function LoginScreen ({navigation}) {
    return(
        <ImageBackground source={require('../images/bg_sb.png')} style={{flex: 1, width: Dimensions.get("window").width, height: Dimensions.get("window").height, resizeMode: 'stretch'}}>
            <View style={{ flex: 1 }}>
                <View style={{flex: 1, alignItems: 'center'}}>
                    <Image source={require('../images/logo.png')} style={{marginTop: 150, width: 350, height: 150, resizeMode: 'contain'}}>
                        </Image>

                    <View>
                        <Icon
                        name='ios-person'
                        size={28} color={'rgba(255, 255, 255, 0.7)'} style={{position: 'absolute', top: 8, left: 37,}}
                        />
                    <TextInput
                        
                        style={{width: 350, height: 50, borderRadius: 25, fontSize: 16, paddingLeft: 45, backgroundColor: 'rgba(0, 0, 0, 0.35)', color: 'rgba(255, 255, 255, 0.7)', marginHorizontal: 25,}}
                        placeholder={'Usuario'}
                        placeholderTextColor={'rgba(255, 255, 255, 0.7)'}
                        underlineColorAndroid='transparent'
                        >
                                
                    </TextInput>
                    </View>

                    <View style={{marginTop:10}}>
                    <Icon
                        name='ios-key'
                        size={28} color={'rgba(255, 255, 255, 0.7)'} style={{position: 'absolute', top: 8, left: 37,}}
                        />
                    <TextInput
                        style={{
                        width: 350, 
                        height: 50, 
                        borderRadius: 25, 
                        fontSize: 16, 
                        paddingLeft: 45, 
                        backgroundColor: 'rgba(0, 0, 0, 0.35)', 
                        color: 'rgba(255, 255, 255, 0.7)', 
                        marginHorizontal: 25,}}
                        placeholder={'Contraseña'}
                        placeholderTextColor={'rgba(255, 255, 255, 0.7)'}
                        underlineColorAndroid='transparent'
                        secureTextEntry={true}
                        />
                    </View>
                    
                    <TouchableOpacity
                        style={{
                        marginTop: 20, 
                        width: 350, 
                        height: 50, 
                        borderRadius: 25, 
                        backgroundColor: '#bd3a3a',}}
                        onPress={() => navigation.navigate('HomeApp') }
                        >
                    <Text style={{color: 'white', fontSize: 20, textAlign: 'center', marginTop: 13}}>Ingresar</Text>
                    </TouchableOpacity>

                </View>
            </View>
        </ImageBackground>
    );
}