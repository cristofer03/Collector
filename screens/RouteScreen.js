import React, { useEffect, useState } from 'react';
import { ActivityIndicator, View } from 'react-native';
import { Body, 
         Button, 
         Text, 
         List, 
         ListItem, 
         Left,
         Right,
         Icon,
         } from 'native-base';
import CustomHeader from '../components/CustomHeader';
import { FlatList, TouchableOpacity  } from 'react-native-gesture-handler';
     



function Item({ name, email, location }) {
    return (
            <ListItem icon style={{height: 100, margin: 2 , backgroundColor: '#E5E7E9', marginLeft: 5}}>
                <Left style={{paddingLeft: 15}}>
                <Button style={{ backgroundColor: "#95A5A6" }}>
                    <Icon active name="md-cash" />
                </Button>
                </Left>
                <Body style={{height: 100, backgroundColor: '#E5E7E9'}}>
                <Text>{name}</Text>
                <Text note>{email}</Text>
                </Body>
                <Right style={{height:100, width: 120,backgroundColor:"#95A5A6"  }}>
                    <Text style={{paddingLeft: 15, color: 'red'}}>{location}</Text>
                </Right>
          </ListItem>
    );
  }
  


export default function RouteScreen ({navigation}) {

    const [isLoading, setLoading] = useState(true);
    const [data, setData] = useState([]);
    
    
    console.log(data);
  
    useEffect(() => {
      fetch('https://randomuser.me/api?results=20')  //https://jsonplaceholder.typicode.com/photos
        .then((response) => response.json())
        .then((json) => setData(json.results))
        .catch((error) => console.error(error))
        .finally(() => setLoading(false));
    }, []);


   const handlePress = ({item}) => {
        navigation.navigate('SalesDetail', {

           location: item.location.postcode,
           name: item.name.first,
           email:item.email, 
           cell:item.cell,
        })
       
        // console.log(location);
    

    }

    const renderSeparator = () => {
        return(
            <View style={{height: 1, width: '100%', backgroundColor:'black'}}></View>
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor:'#fff'}}>
            <CustomHeader title= 'VENTAS' isHome={true} navigation={navigation}/>
                <View style={{marginBottom: 80, marginTop: 5}}>
                <List>
                {isLoading ? <ActivityIndicator animating size= 'large'/> : (
                    <FlatList
                        data={data}
                        renderItem={({ item, index }) => (                   
                            <TouchableOpacity onPress={() => handlePress({item})}
                                              activeOpacity={0.5}
                            >
                                <Item
                                    name={item.name.first}
                                    email={item.email}
                                    location={item.location.postcode}
                                />   
                            </TouchableOpacity>               
                        )
                        }
                        keyExtractor={(item, index) => index.toString()} 
                        ItemSeparatorComponent={renderSeparator}                                   
                    />
                )}
                </List>
                </View>
        </View>
    );
}
