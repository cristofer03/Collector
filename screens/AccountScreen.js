import React, { useState } from 'react'
import { View, Text, SafeAreaView, Alert, TextInput, StyleSheet, ScrollView } from 'react-native'
import { Dropdown } from "react-native-material-dropdown"
import CustomHeader from '../components/CustomHeader'
import banca from '../banca.json'
import { TouchableOpacity } from 'react-native-gesture-handler'
//import ScreenModal from '../components/ScreenModal'


export default function AccountScreen({navigation}) {
    console.log('recibiendo datos de navegacion', banca);

    const [ddlSelectedValue, setDdlSelectedValue] = useState();
    const [add, setText] = React.useState('');
    const [quit, setChange] = React.useState('');
    const [mount, setMount] = React.useState(0);
    const [maxMount, setMaxMount] = React.useState(100000);
    const [isBackground, setIsBackground] = React.useState(true);
    const [isBackgroundText, setIsBackgroundText] = React.useState(true);

    console.log('recibo el limite maximo', maxMount);



    const sumValue = (e) => {  
        if(add != ''){
            Alert.alert(
                "Confirmar",
                "Esta seguro de agregar este monto",
                [
                  {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                  },
                  { text: "OK", onPress: () => {
                    const newMount = parseFloat(e);
                    const suma = mount+newMount;
                    setMount(suma);
                    setText('');
                    if((suma) >= (maxMount - 10000)){
                        setIsBackground(false);
                        setIsBackgroundText(false);
                        Alert.alert(
                            "Aviso",
                            "ESTA PROXIMO A ALCANZAR EL LIMITE MAXIMO POR FAVOR VISITE EL BANCO",
                            [
                              {
                                text: "Cancel",
                                onPress: () => console.log("Cancel Pressed"),
                                style: "cancel"
                              },
                              { text: "OK"}
                            ],
                            { cancelable: false }
                        )  
                    }
                }
                }
                ],
                { cancelable: false }
            )
        }else {
            Alert.alert(
                "Campo vacio",
                "",
                [
                  {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                  },
                  { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
              );
          
        }
    }

    const restValue = (d) => {
        if(quit != ''){
            Alert.alert(
                "Confirmar",
                "Esta seguro de restar este monto",
                [
                  {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                  },
                  { text: "OK", onPress: () => {
                    const newMount = parseFloat(d);
                    const rest = mount - newMount;
                    setMount(rest);
                    setChange(''); 

                    if((rest) <= maxMount){
                        setIsBackground(true);
                    }
                  }
                }
                ],
                { cancelable: false }
            )
        }else {
            Alert.alert(
                "Campo vacio",
                "",
                [
                  {
                    text: "Cancel",
                    onPress: () => console.log("Cancel Pressed"),
                    style: "cancel"
                  },
                  { text: "OK", onPress: () => console.log("OK Pressed") }
                ],
                { cancelable: false }
              );
          
        }
    }

    const changeSelectedStateValue = (ddlValue) =>{
        setDdlSelectedValue(ddlValue)
        console.log('ddlValue: ' + ddlValue)
        } 

    return(
        <SafeAreaView style={styles.container}>
        <CustomHeader title= 'RECOLECTOR' navigation={navigation}/>
            <View style={{  backgroundColor: isBackground ? 'white' : 'yellow', height: 50, width:100, borderRadius: 5 }}>
                <Text style={{textAlign:'center', color: isBackgroundText ? 'gray' : 'black'}}>Limite Max.</Text>
                <Text style={{textAlign:'center',padding: 5, fontWeight: 'bold'}}>{maxMount}</Text>
            </View>
         <ScrollView>
            <View style={styles.list}>
                {/* <Text style={{fontSize: 18, color:'#ffff'}}>ORGANIZACION</Text> */}
                <Text style={{fontSize: 16, marginTop: 0, color:'#ffff'}}>BALANCE</Text>

                <View style={styles.sale}>
                    <Text style={{textAlign: 'center', fontSize: 32, color: 'blue'}}>{mount} $RD</Text>
                </View>

                    
                <Text style={styles.text1}>ACCION COBRADOR</Text>
                <Dropdown 
                    data={banca}
                    value={ddlSelectedValue} 
                    containerStyle={{
                                     height:50, 
                                     width:250, 
                                     backgroundColor: 'white', 
                                     justifyContent:'center',
                                     borderRadius: 5,
                                     paddingHorizontal:15,
                                     marginTop: 10
                                    }}
                    label='BANCA'
                    itemColor={'red'} 
                    useNativeDriver={true}
                    dropdownPosition={1} 
                    onChangeText={(value,index,data)=>
                    changeSelectedStateValue(value)
                }    
                />

                <Text style={styles.text1}>MONTO RECIBIDO</Text>
                <View style={{flexDirection:'row', marginTop: 10}}>
                    <TextInput
                        placeholder={'MONTO RECOLECTADO'}
                        keyboardType={"numeric"}
                        style={{ height: 50, width: 200, backgroundColor: 'white', borderRadius: 5, marginTop: 2, paddingHorizontal:15}}
                        onChangeText={text => setText(text)}
                        value={add}
                    />
                    <TouchableOpacity style={styles.addButton}
                                      onPress={()=> sumValue(add)}
                    >
                        <Text>Add</Text>
                    </TouchableOpacity>
                </View>


                <Text style={styles.text1}>ENTREGAR DINERO</Text>
                <View style={{flexDirection:'row', marginTop: 10}}>
                    <TextInput
                        placeholder={'MONTO ENTREGADO'}
                        keyboardType={"numeric"}
                        style={{ height: 50, width: 200, backgroundColor: 'white', borderRadius: 5, marginTop: 2, paddingHorizontal:15}}
                        onChangeText={text => setChange(text)}
                        value={quit}
                    />
                    <TouchableOpacity style={styles.addButton}
                                      onPress={()=> restValue(quit)}
                    >
                        <Text>Add</Text>
                    </TouchableOpacity>
                </View>
                <TouchableOpacity style={styles.transact}
                                  onPress={() => navigation.navigate('AccountDetail')}
                >
                        <Text style={{color:'white', justifyContent:'center', textAlign:'center'}}>TRANSACCIONES</Text>
                </TouchableOpacity>
            </View>
         </ScrollView>
     </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        backgroundColor: '#393939'
    },
    list: {
        flex: 1, 
        alignItems: 'center', 
        paddingTop: 10
    },
    sale: {
        backgroundColor: '#E5E7E9', 
        justifyContent: 'center', 
        width: 250, 
        height: 50, 
        marginTop: 10, 
        borderRadius: 5
    },
    balance: {
        width: 250, 
        height: 50, 
        backgroundColor: '#E5E7E9', 
        marginTop: 10, 
        justifyContent: 'center', 
        borderRadius: 5
    },
    text1: {
        textAlign: 'center', 
        fontSize: 14, 
        marginTop: 15,
        color:'#ffff'
    },
     text2: {
        textAlign: 'center', 
        fontSize: 16,
        color: 'blue',
     },
     addButton: {
        backgroundColor:'#D1CFD2',
        width: 50, 
        height: 50, 
        borderRadius: 5, 
        marginTop: 2, 
        justifyContent:'center', 
        paddingLeft: 10, 
        borderColor:'red', 
        borderWidth: 0.5
     },

     transact : {
        backgroundColor:'#bd3a3a', 
        width: 250, 
        height: 50, 
        borderRadius: 5, 
        marginTop: 20, 
        justifyContent:'center', 
        paddingLeft: 10, 
        borderColor:'red', 
        borderWidth: 0.5
     }
})


