import * as React from 'react';
import { Text, View, Image, SafeAreaView, TouchableOpacity  } from 'react-native';
import { NavigationContainer} from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createStackNavigator } from '@react-navigation/stack';
import LoginScreen from './screens/LoginScreen';
import RegisterScreen from './screens/RegisterScreen';
import HomeScreen from './screens/HomeScreen';
import RouteScreen from './screens/RouteScreen';
import AccountScreen from './screens/AccountScreen';
import HomeScreenDetail from './screens/HomeScreenDetail';
import RoutesScreenDetail from './screens/RoutesScreenDetail';
import AccountScreenDetail from './screens/AccountScreenDetail';
import { createDrawerNavigator } from '@react-navigation/drawer';





function CustomDrawerContent(props) {
  return (
    <SafeAreaView style={{flex: 1}}>
      <View style={{height: 150, alignItems: 'center', justifyContent: 'center', backgroundColor: '#393939'}}>
        <Image style={{height: 120, width: 120, borderRadius: 60}}
        source={require('./images/sb.png')}
        />
      </View>
      <View
       style={{marginLeft: 15}}>
        <TouchableOpacity
                  style={{marginTop: 20}}
                  onPress={() => props.navigation.navigate('MenuTab')}
                  >
                    <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image style={{width: 22, height: 22, marginLeft: 5}}
                                   source={require('./images/home.png')}
                                   resizeMode= 'contain'
                        />
                      <Text style={{fontSize: 18, 
                                    marginLeft: 10, 
                                    color: 'red', 
                                    fontWeight:'bold'}}>MENU</Text>
                    </View>
        </TouchableOpacity>

        <TouchableOpacity
                  style={{marginTop: 20}}
                  onPress={() => props.navigation.navigate('Sales')}
                  >
                  <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image style={{width: 22, height: 22, marginLeft: 5}}
                                   source={require('./images/cash.png')}
                                   resizeMode= 'contain'
                        />
                      <Text style={{fontSize: 18, marginLeft: 10, color: 'red', fontWeight:'bold'}}>VENTAS</Text>
                    </View>
        </TouchableOpacity>
        <TouchableOpacity
                  style={{marginTop: 20}}
                  onPress={() => props.navigation.navigate('Cuenta')}
                  >
                    <View style={{flexDirection: 'row', alignContent: 'center'}}>
                    <Image style={{width: 22, height: 22, marginLeft: 5}}
                                   source={require('./images/person.png')}
                                   resizeMode= 'contain'
                        />
                      <Text style={{fontSize: 18, marginLeft: 10, color: 'red', fontWeight:'bold'}}>CUENTA</Text>
                    </View>
        </TouchableOpacity>
        </View>
    </SafeAreaView>
  )
}



const Tab = createBottomTabNavigator();


const navOption = () => ({
  headerShown: false
});

const StackHome = createStackNavigator();

function HomeStack() {
  return(
    <StackHome.Navigator initialRouteName= 'Home'>
      <StackHome.Screen name='Home' component={HomeScreen} options={navOption}/>
      <StackHome.Screen name='HomeDetail' component={HomeScreenDetail} options={navOption} />
    </StackHome.Navigator>
  )
}

const StackRoute = createStackNavigator();

function RouteStack() {
  return(
    <StackRoute.Navigator initialRouteName= 'Sales'>
      <StackRoute.Screen name='Sales' component={RouteScreen} options={navOption} />
      <StackRoute.Screen name='SalesDetail' component={RoutesScreenDetail} options={navOption} />
    </StackRoute.Navigator>
  )
}

const StackAccount = createStackNavigator();

  function AccountStack() {
    return(
      <StackAccount.Navigator initialRouteName= 'Account'>
        <StackAccount.Screen name='Account' component={AccountScreen} options={navOption} />
        <StackAccount.Screen name='AccountDetail' component={AccountScreenDetail} options={navOption}/>
      </StackAccount.Navigator>
    )
  }


  function tabNavigator(){
    return(
        <Tab.Navigator 
            screenOptions={({ route }) => ({
            tabBarIcon: ({ focused, color, size }) => {
            
              let iconName;

              if (route.name === 'Home') {
                iconName = focused
                  ? require('./images/browser.png')
                  : require('./images/home-black.png');
              } else if (route.name === 'Ventas') {
                iconName = focused 
                ? require('./images/cash.png') 
                : require('./images/payment.png');
              }else if(route.name === 'Cuenta') {
                iconName = focused 
                ? require('./images/account-white.png') 
                : require('./images/account-black.png');
              }
             
              return <Image source={iconName} style={{width: 20, height: 20}}
                            resizeMode='contain'
                      />;
            },
          })}
          tabBarOptions={{
            activeTintColor: 'blue',
            inactiveTintColor: 'black',
            activeBackgroundColor: "#393939",
            inactiveBackgroundColor: "#393939"
          }}
        >      
            <Tab.Screen name="Home" component={HomeStack} />
            <Tab.Screen name="Ventas" component={RouteStack} />
            <Tab.Screen name="Cuenta" component={AccountStack} />
        </Tab.Navigator>
    )
  }


const Drawer = createDrawerNavigator();

function DrawerNavigator() {
  return (
      <Drawer.Navigator initialRouteName="MenuTab" 
                        drawerContent={props => CustomDrawerContent(props)}
      >
        <Drawer.Screen name="MenuTab" component={tabNavigator} />
        <Drawer.Screen name="TabSales" component={RouteScreen} />
        <Drawer.Screen name="TabCuenta" component={AccountScreen} />
      </Drawer.Navigator>
  )
}


const StackApp = createStackNavigator();

export default function App() {
  const [fontsLoaded, setFontsLoaded] = React.useState(false);
  
  React.useEffect(() => {
    async function loadResourceAsync() {
      try{
        await Expo.Font.loadAsync({
          Roboto: require("native-base/Fonts/Roboto.ttf"),
          Roboto_medium: require("native-base/Fonts/Roboto_medium.ttf"),
        // await Font.loadAsync({
        //   ...Ionicons.font,
        //   'roboto-medium': require('./node_modules/native-base/Fonts/Roboto_medium.ttf'),
        });
      }catch (e){
        console.warn(e);
      }finally{
        setFontsLoaded(true);
      }
    }

    loadResourceAsync();
}, []);

if (!fontsLoaded){
  return(null);
}
    return (
      <NavigationContainer>
        <StackApp.Navigator initialRouteName= 'Login'>
          <StackApp.Screen name='HomeApp' component={DrawerNavigator} options={navOption}/>
          <StackApp.Screen name='Login' component={LoginScreen} options={navOption} />
          <StackApp.Screen name='Register' component={RegisterScreen} options={navOption} />
        </StackApp.Navigator>
      </NavigationContainer>
    );
}