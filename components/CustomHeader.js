import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';


export default function CustomHeader({title, isHome, navigation}) {
    return(
        <View style={{flexDirection: 'row', height: 75, paddingTop: 5, backgroundColor:'#bd3a3a', justifyContent: 'center' }}>
                <View style={{flex: 1, justifyContent: 'center'}}>
                    {
                        isHome?
                        <TouchableOpacity onPress={() => navigation.openDrawer()}>
                            <Image style={{width: 30, height: 30, marginLeft: 20}}
                                   source={require('../images/open-menu.png')}
                                   resizeMode= 'contain'
                        />
                        </TouchableOpacity>
                        :
                        <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center' }}
                        onPress={() => navigation.goBack()}   
                        >
                            <Image style={{ width: 20, height: 20, marginLeft: 15 }}
                            source={require('../images/return.png')}
                            resizeMode='contain'
                            />
                            <Text style={{padding: 5, color: '#ffff'}}>Atras</Text>
                        </TouchableOpacity>
                    }
                </View>
            <View style={{flex: 1.5,justifyContent:'center'}}>
                <Text style={{textAlign: 'center', color: '#ffff'}}>{title}</Text>
            </View>
            <View style={{flex: 1, justifyContent: 'center'}}>
                <TouchableOpacity style={{flexDirection: 'row', alignItems: 'center', paddingHorizontal: 50 }}
                                onPress={() => navigation.navigate('Login')}
                                >
                    <Image style={{ width: 20, height: 20 }}
                                source={require('../images/logout.png')}
                                resizeMode='contain'
                                />
                </TouchableOpacity>
            </View>
        </View>
    )
}