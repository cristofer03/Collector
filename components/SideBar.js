import React, {Component} from 'react';
import { View,StyleSheet,Button,TextInput} from 'react-native';

export default class App extends Component {
  constructor(props) {
  super(props);
  this.state = { text: 'My Text' };
  }

  clearText(){
    this.setState({text:''})
  }
    render() {
     return (
       <View style={styles.container}>
         <TextInput
           style={{height: 40,backgroundColor: 'white',width:300}}
           onChangeText={(text) => this.setState({text})}
           value={this.state.text}
         />
       <Button
         onPress={()=>this.clearText()}
         title="Clear"
         />
       </View>
       );
   }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#ddd',
  },
});